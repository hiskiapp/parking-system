## Parking System

Made with IntelliJ IDEA

Members List:
- Hiskia Anggi Puji Pratama (A11.2020.12730)
- Muhammad Ariq Pratama (A11.2020.12944)
- Moh. Arda Fadli Robby (A11.2020.13087)

To do List:
- [x] Initial Commit
- [x] UI Design
- [x] Class Diagram
- [x] CRUD System
- [x] Print Data