import javax.swing.*;
import java.sql.Connection;
import java.sql.DriverManager;

public class Conn {

    static final String JDBC_DRIVER = "com.mysql.cj.jdbc.Driver";
    static final String DB_URL = "jdbc:mysql://localhost/parkingsystem";
    static final String USER = "root";
    static final String PASS = "";

    public static Connection main() {
        try{
            Class.forName(JDBC_DRIVER);

            return DriverManager.getConnection(DB_URL, USER, PASS);
        }catch(Exception e){
            JOptionPane.showMessageDialog(null, e);
        }
        return null;
    }
}
