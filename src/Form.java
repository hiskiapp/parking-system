import javax.swing.*;
import javax.swing.table.DefaultTableModel;
import java.awt.*;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.sql.*;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Objects;

public class Form extends JFrame {
    private JButton btnCheckin;
    private JButton btnCheckout;
    private JButton btnEdit;
    private JButton btnDelete;
    private JButton btnPrint;
    private JTable tblMain;
    private JTextField txtPoliceNumber;
    private JTextField txtTime;
    private JTextField txtFee;
    private JPanel main;
    private JLabel lblTitle;
    private JLabel lblDateDesc;
    private JLabel lblPoliceNumber;
    private JLabel lblTransportationType;
    private JLabel lblTimeDesc;
    private JLabel lblCostDesc;
    private JComboBox cbxTransportationType;
    private JScrollPane jspMain;
    private JLabel lblTime;
    private JLabel lblDate;
    private JLabel lblRp;
    private JLabel lblFee;
    private JButton btnClear;

    private int selectedId;

    public Form() {
        setContentPane(main);
        timer();
        clear();
        datatable();

        tblMain.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent e) {
                int id = Integer.parseInt(tblMain.getValueAt(tblMain.getSelectedRow(),0).toString());
                find(id);
                btnCheckin.setEnabled(false);
            }
        });

        btnCheckin.addActionListener(e -> {
            if(isFormValid()) {
                checkin();
                clear();
                datatable();
            }
        });

        btnCheckout.addActionListener(e -> {
            checkout();
            clear();
            datatable();
        });

        btnEdit.addActionListener(e -> {
            edit();
            clear();
            datatable();
        });

        btnDelete.addActionListener(e -> {
            delete();
            clear();
            datatable();
        });

        btnPrint.addActionListener(e -> {
            try {
                print();
            } catch (SQLException ex) {
                throw new RuntimeException(ex);
            }
        });

        btnClear.addActionListener(e -> {
            clear();
        });
    }

    public void timer() {
        Timer t = new Timer(500, e -> {
            Date d = new Date();
            lblDate.setText(DateFormat.getDateInstance(DateFormat.FULL).format(d));
            lblTime.setText(DateFormat.getTimeInstance(DateFormat.SHORT).format(d));
        });
        t.setRepeats(true);
        t.setCoalesce(true);
        t.setInitialDelay(0);
        t.start();
    }

    public int parkingFee(String type) {
        return switch (type) {
            case "Motor" -> 2000;
            case "Mobil" -> 4000;
            case "Truk" -> 5000;
            default -> 0;
        };
    }

    public void datatable(){
        DefaultTableModel model = new DefaultTableModel();

        model.addColumn("ID");
        model.addColumn("No Pol");
        model.addColumn("Tanggal");
        model.addColumn("Jam Masuk");
        model.addColumn("Jam Keluar");
        model.addColumn("Biaya");
        model.addColumn("Jenis Kendaraan");

        try {
            Connection conn = Conn.main();
            assert conn != null;
            Statement stmt = conn.createStatement();

            ResultSet rs = stmt.executeQuery("SELECT * FROM parkir ORDER BY id DESC");
            while(rs.next())
            {
                model.addRow(new Object[] {
                        rs.getString("id"),
                        rs.getString("nopol"),
                        rs.getString("tanggal"),
                        rs.getString("jam_masuk"),
                        rs.getString("jam_keluar"),
                        rs.getString("biaya_parkir"),
                        rs.getString("tipe"),
                });
            }

            stmt.close();
            conn.close();

            tblMain.setModel(model);
        }
        catch(Exception e) {
            e.printStackTrace();
        }
    }

    public void checkin() {
        try {
            Connection conn = Conn.main();
            assert conn != null;
            Statement stmt = conn.createStatement();

            String nopol = txtPoliceNumber.getText();
            String tanggal = new SimpleDateFormat("yyyy-MM-dd").format(new Date());
            String jam_masuk = new SimpleDateFormat("HH:mm:ss").format(new Date());
            String tipe = Objects.requireNonNull(cbxTransportationType.getSelectedItem()).toString();

            String sql = "INSERT INTO parkir (nopol, tanggal, jam_masuk, tipe) VALUES (?,?,?,?)";
            PreparedStatement pstmt = conn.prepareStatement(sql);
            pstmt.setString(1, nopol);
            pstmt.setString(2, tanggal);
            pstmt.setString(3, jam_masuk);
            pstmt.setString(4, tipe);
            pstmt.execute();

            stmt.close();
            conn.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void find(int id){
        try {
            Connection conn = Conn.main();
            assert conn != null;
            Statement stmt = conn.createStatement();

            String sql = "SELECT * FROM parkir WHERE id = ?";
            PreparedStatement pstmt = conn.prepareStatement(sql);
            pstmt.setInt(1, id);

            ResultSet rs = pstmt.executeQuery();
            rs.next();

            selectedId = id;

            if(rs.getString("jam_keluar") == null) {
                txtPoliceNumber.setEnabled(true);
                cbxTransportationType.setEnabled(true);
                btnCheckout.setEnabled(true);
                btnEdit.setEnabled(true);

                String fee = String.valueOf(feeTotal(rs.getString("tipe"), rs.getString("tanggal") + " " + rs.getString("jam_masuk")));
                lblFee.setText(fee);
            } else {
                txtPoliceNumber.setEnabled(false);
                cbxTransportationType.setEnabled(false);
                btnCheckout.setEnabled(false);
                btnEdit.setEnabled(false);
                lblFee.setText(rs.getString("biaya_parkir"));
            }

            txtPoliceNumber.setText(rs.getString("nopol"));
            cbxTransportationType.setSelectedItem(rs.getString("tipe"));
            btnDelete.setEnabled(true);
            btnPrint.setEnabled(true);

            stmt.close();
            conn.close();
        } catch(Exception e) {
            e.printStackTrace();
        }

    }

    public void checkout() {
        try {
            Connection conn = Conn.main();
            assert conn != null;
            Statement stmt = conn.createStatement();

            PreparedStatement ps = conn.prepareStatement("SELECT * FROM parkir WHERE id = ?");
            ps.setInt(1, selectedId);

            ResultSet rs = ps.executeQuery();
            rs.next();

            String jam_keluar = new SimpleDateFormat("HH:mm:ss").format(new Date());
            int biaya_parkir = feeTotal(rs.getString("tipe"), rs.getString("tanggal") + " " + rs.getString("jam_masuk"));

            PreparedStatement ps2 = conn.prepareStatement("UPDATE parkir SET jam_keluar=?,biaya_parkir=? WHERE id=?");
            ps2.setString(1, jam_keluar);
            ps2.setInt(2, biaya_parkir);
            ps2.setInt(3, selectedId);

            ps2.execute();

            stmt.close();
            conn.close();
        } catch(Exception e) {
            e.printStackTrace();
        }
    }

    public void edit(){
        try {
            Connection conn = Conn.main();
            assert conn != null;
            Statement stmt = conn.createStatement();

            String nopol = txtPoliceNumber.getText();
            String tipe = Objects.requireNonNull(cbxTransportationType.getSelectedItem()).toString();

            String sql = "UPDATE parkir SET nopol=?, tipe=? WHERE id=?";
            PreparedStatement pstmt = conn.prepareStatement(sql);
            pstmt.setString(1, nopol);
            pstmt.setString(2, tipe);
            pstmt.setInt(3, selectedId);
            pstmt.execute();

            stmt.close();
            conn.close();
        } catch(Exception e) {
            e.printStackTrace();
        }
    }

    public void delete(){
        try {
            Connection conn = Conn.main();
            assert conn != null;
            Statement stmt = conn.createStatement();

            String sql = "DELETE FROM parkir WHERE id=?";
            PreparedStatement pstmt = conn.prepareStatement(sql);
            pstmt.setInt(1, selectedId);
            pstmt.execute();

            stmt.close();
            conn.close();
        } catch(Exception e) {
            e.printStackTrace();
        }
    }

    public void clear(){
        txtPoliceNumber.setText("");
        cbxTransportationType.setSelectedIndex(0);
        lblFee.setText("0");
        txtPoliceNumber.setEnabled(true);
        cbxTransportationType.setEnabled(true);
        btnCheckin.setEnabled(true);
        btnEdit.setEnabled(false);
        btnCheckout.setEnabled(false);
        btnDelete.setEnabled(false);
        btnPrint.setEnabled(false);
        selectedId = 0;
    }

    public boolean isFormValid(){
        if(txtPoliceNumber.getText().isEmpty()){
            JOptionPane.showMessageDialog(null, "Nopol harus diisi");
            txtPoliceNumber.requestFocus();

            return false;
        }
        else if(cbxTransportationType.getSelectedIndex() == 0){
            JOptionPane.showMessageDialog(null, "Tipe kendaraan harus diisi");
            cbxTransportationType.requestFocus();

            return false;
        }

        return true;
    }

    public int feeTotal(String type, String datetime) {
        try {
            int fee_per_hour = parkingFee(type);
            double time = new Date().getTime() - new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").parse(datetime).getTime();
            double time_minute = time / 1000 / 60;
            int final_hour = (int) Math.ceil(time_minute / 60);

            return final_hour * fee_per_hour;
        } catch (ParseException ignored) {
            return 0;
        }
    }

    public void print() throws SQLException {
        Invoice invoice = new Invoice(selectedId);
        invoice.setPreferredSize(new Dimension(700, 400));
        invoice.pack();
        invoice.setLocationRelativeTo(null);
        invoice.setVisible(true);
    }

    public static void main(String[] args) {
        Form form = new Form();
        form.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        form.setPreferredSize(new Dimension(700, 400));
        form.pack();
        form.setLocationRelativeTo(null);
        form.setVisible(true);
    }



}
