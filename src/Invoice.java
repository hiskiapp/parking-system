import javax.swing.*;
import java.awt.*;
import java.sql.*;

public class Invoice extends JFrame {
    private JPanel main;
    private JLabel lblId;
    private JLabel lblPoliceNumber;
    private JLabel lblType;
    private JLabel lblDate;
    private JLabel lblCheckin;
    private JLabel lblCheckout;
    private JLabel lblFee;

    public Invoice(int id) throws SQLException {
        setContentPane(main);
        find(id);
    }

    public void find(int id) throws SQLException {
        Connection conn = Conn.main();
        assert conn != null;
        Statement stmt = conn.createStatement();

        String sql = "SELECT * FROM parkir WHERE id = ?";
        PreparedStatement ps = conn.prepareStatement(sql);
        ps.setInt(1, id);
        ResultSet rs = ps.executeQuery();

        while (rs.next()) {
            lblId.setText(rs.getString("id"));
            lblPoliceNumber.setText(rs.getString("nopol"));
            lblType.setText(rs.getString("tipe"));
            lblDate.setText(rs.getString("tanggal"));
            lblCheckin.setText(rs.getString("jam_masuk"));
            if(rs.getString("jam_keluar") != null) {
                lblCheckout.setText(rs.getString("jam_keluar"));
            } else {
                lblCheckout.setText("-");
            }
            if(rs.getString("biaya_parkir") != null) {
                lblFee.setText(rs.getString("biaya_parkir"));
            } else {
                lblFee.setText("-");
            }
        }
    }
}
