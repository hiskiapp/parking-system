import javax.swing.*;
import java.awt.*;

public class Main {
    public static void main(String[] args) {
        Form form = new Form();
        form.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        form.setPreferredSize(new Dimension(700, 400));
        form.pack();
        form.setLocationRelativeTo(null);
        form.setVisible(true);
    }
}